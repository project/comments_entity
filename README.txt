Drupal core comment module is great for commenting on nodes.

This module provides fieldable comments for any entity even comments itself.
Entity Comments integrates with views and rules.

Installation

Simply enable the module and go to "admin/config/content/comments_entity" and
enable Entity Comments on any entity.

Features

Enable comments for any entity.
Any entity may be the author of a comment. Defaults to user.
Keeps track of thread.
Keeps track of comment thread depth.
Keeps track of the origin of the comments, no matter where in the comment tree.
Approval workflow
Keeps track who approved comment
Keeps track who deleted comment
Entity API integration.
Rules integration.
Views integration.
Reverse Proxy & other caching

Comment forms are loaded and submit by jQuery ajax in resemblence to comments at
facebook & twitter.
Comments themselves are also loaded by JQuery ajax so commenting will work well
despite global caching methods.
Using the Cache Exclude module, you can disable caching for these callbacks.
Security

Uses Entity Access to grant permissions to view,create,delete, approve & purge
comments.
Uses Form Api to process new comments.

Troubleshooting

To enable replies to comments, enable entity comments for entity comments. :)

Similar Modules

Reply uses the field api to attach comment functionality to entities.
Does not allow any entity to be the author of a comment.
Main difference is that Comments Entity provides commenting without the use
of the field api. So you must not create new fields on existing entities to
enable comments. You must not load all comments just to load an entity which
happens to have comments.

Comments Entity hooks into hook_entity_view and uses a global configuration to
display comment form, list and link. The Comments Entity is by itself fieldable
and you may attach any field to it, such as an image field. Comments Entity
also relies mainly heavily on javascript to manage the inline creation of
comments.