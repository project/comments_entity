<div id="comments_entity-<?php print $comments_entity->comment_id; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <header>
    <?php print $avatar; ?>
    <?php print $author; ?>
  </header>
  <?php print render($content); ?>
  <footer>
    <time datetime="<?php print $comment_datetime; ?>"><?php print $comment_date; ?></time>
  </footer>
</div>