<?php

/**
 * @file
 * Describe theme function callbacks.
 */

function theme_comments_entity_form_wrapper($variables) {
  $output = '';

  if(entity_access('create', 'comments_entity', $variables['entity'])) {

    $entity_type = $variables['entity_type'];
    $entity_id = $variables['entity_id'];
    $entity = $variables['entity'];
    $comments_entity_type = $variables['comments_entity_type'];
    $settings = $variables['settings'];

    $wrapper_id = "comments_entity-" . $comments_entity_type . "-wrapper-" . $entity_type . "-" . $entity_id;

    if($entity_type == 'comments_entity' && $variables['count'] > 0) {
      $settings['display_comment_form'] = 'show';
    }
    elseif($entity_type == 'comments_entity') {
      $settings['display_comment_form'] = 'hide';
    }

    $variables['attributes']['id'] = $wrapper_id . '-form';
    $variables['attributes']['class'][] = 'comments_entity-form-wrapper';
    $variables['attributes']['data-display-comment-form'] = $settings['display_comment_form'];

    $output .= '<div ' . drupal_attributes($variables['attributes']) . '>';

    if($settings['display_comment_form'] == 'show') {

      $attach_attributes = array(
        'entity_type' => $variables['entity_type'],
        'entity_id' => $variables['entity_id'],
        'reply_id' => $entity_type == 'comments_entity' ? $entity->reply_id : 0,
        'thread_id' => $entity_type == 'comments_entity' ? $entity->thread_id : 0,
      );

      $output .= '<img onload="Drupal.behaviors.comments_entityFormLoad.attach(' . htmlspecialchars(json_encode($attach_attributes), ENT_QUOTES, 'UTF-8') . ')" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />';
    }

    $output .= '</div>';
  }

  return $output;
}

function theme_comments_entity_list($variables) {
  $output = '';

  $entity_type = $variables['entity_type'];
  $entity_id = $variables['entity_id'];
  $entity = $variables['entity'];
  $comments_entity_type = $variables['comments_entity_type'];

  $wrapper_id = 'comments_entity-' . $comments_entity_type . '-wrapper-' . $entity_type . '-' . $entity_id;

  $variables['attributes']['id'] = $wrapper_id . '-comments_entity';
  $variables['attributes']['class'][] = 'comments_entity-comments_entity-wrapper';

  $output = '<div' . drupal_attributes($variables['attributes']) . '>';

  if ($variables['count'] == 0) {
    $output .= '<div class="view-content"></div>';
  }
  else {

    $load_attributes = array(
      'class' => array('comments_entity-load'),
      'data-comments-entity-type' => $comments_entity_type,
      'data-entity-type' => $variables['entity_type'],
      'data-entity-id' => $variables['entity_id'],
      'data-display' => 'default',
    );

    $attach_attributes = array(
      'comments_entity_type' > $comments_entity_type,
      'entity_type' => $variables['entity_type'],
      'entity_id' => $variables['entity_id'],
      'display' => 'default',
    );

    $output .= '<div' . drupal_attributes($load_attributes) . '><div>';

    if($variables['count'] > 0 || $entity_type !== 'comments_entity') {
      $output .= t('Loading comments...') . '</div>';
      $output .= '<img onload="Drupal.behaviors.comments_entityLoad.attach(' . htmlspecialchars(json_encode($attach_attributes), ENT_QUOTES, 'UTF-8') . ')" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />';
    }
    elseif($variables['count'] > 2) {
      $link_text = t('View @count more comments_entity', array('@count' => $variables['count']));
      $entity_uri = entity_uri($entity_type, $entity);
      $output .= l(
        $link_text,
        $entity_uri['path'],
        array(
          'class' => array('comments_entity-load-link'),
          'onclick' => "Drupal.behaviors.comments_entityLoad.attach(' . htmlspecialchars(json_encode($attach_attributes), ENT_QUOTES, 'UTF-8') . ')",
        )
      );
    }

    $output .= '<div class="view-content"></div></div>';

  }

  $output .= '</div>';
  return $output;
}

function theme_comments_entity_link($variables) {
  $entity = $variables['entity'];
  $entity_id = $variables['entity_id'];
  $type = $variables['entity_type'];
  $comments_entity_type = $variables['comments_entity_type'];

  $reply_id = 0;
  $thread_id = !empty($entity->thread_id) ? $entity->thread_id : 0;

  if($type == 'comments_entity') {
    $reply_id = $entity->comment_id;

    if($entity->thread_id == 0) {
      $thread_id = $entity->comment_id;
    }
  }

  $comment_action_label = comment_action_label($entity, $type);
  $entity_uri = entity_uri($type, $entity);

  $link = l(
    $comment_action_label,
    $entity_uri['path'],
    array(
      'html' => TRUE,
      'query' => array(
        'comments_entity' => 'show',
      ),
      'attributes' => array(
        'class' => array('comments_entity-link'),
        'data-comments-entity-type' => $comments_entity_type,
        'data-entity-type' => $type,
        'data-entity-id' => $entity_id,
        'data-reply-id' => $reply_id,
        'data-thread-id' => $thread_id,
        'title' => t('@label on @entity_label', array(
          '@label' => $comment_action_label,
          '@count' => $variables['count'],
          '@entity_label' => entity_label($type, $entity)
        ))
      )
    )
  );

  return $link;
}