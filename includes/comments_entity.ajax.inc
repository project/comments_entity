<?php

/**
 * @file
 * Describe ajax callbacks.
 */

function comments_entity_form_load_callback($comments_entity_type, $entity_type, $entity_id, $replid_id = 0) {

  $data = array(
    'content' => '',
    'success' => FALSE,
  );

  try {
    $form = drupal_get_form(
      'comments_entity_'. $comments_entity_type . '_' . $entity_type.'_'.$entity_id,
      $comments_entity_type,
      $entity_type,
      $entity_id,
      $replid_id,
      false
    );
    $data['content'] = render($form);
    $data['success'] = TRUE;
  }
  catch(Exception $e) {
    $data['content'] = $e->getMessage();
  }

  drupal_json_output($data);
}

function comments_entity_list_load_callback($display, $entity_type, $entity_id) {
  $data = array(
    'content' => '',
    'success' => FALSE,
  );

  $data['content'] = views_embed_view('comments_entity', $display, $entity_type, $entity_id);
  $data['success'] = TRUE;

  drupal_json_output($data);
}
