<?php

/**
 * @file
 * Describe entity and controller classes for entity, entity type and admin UI.
 */

/**
 * Comments Entity class.
 */
class CommentsEntity extends Entity {
  /**
   * Define the label of the entity.
   */
  protected function defaultLabel() {
    return t('Comment #@comment_id', array('@comment_id' => $this->comment_id));
  }

  /**
   * Specify the default uri, which is picked up by uri() by default.
   */
  protected function defaultUri() {

    $path = '';

    if ($this->thread_id > 0 && ($thread_comment = comments_entity_load($this->thread_id))) {
      $entity = entity_load_single($thread_comment->entity_type, $thread_comment->entity_id);

      if($thread_comment->entity_type !== 'comments_entity') {
        $entity_uri = entity_uri($thread_comment->entity_type, $entity);
        $path = $entity_uri['path'] . '#comments_entity-' . $this->comment_id;
      }
      else {
        $entity = $thread_comment;
        $entity_uri = entity_uri($thread_comment->entity_type, $entity);
        $path = $entity_uri['path'];
      }

    }
    else if($this->reply_id > 0) {
      $path = 'comments_entity/' . $this->reply_id . '#comments_entity-' . $this->comment_id;
    }

    if(empty($path) && $this->entity_type !== 'comments_entity') {
      $entity = entity_load_single($this->entity_type, $this->entity_id);
      $entity_uri = entity_uri($this->entity_type, $entity);
      $path = $entity_uri['path'] . '#comments_entity-' . $this->comment_id;
    }
    elseif(empty($path)) {
      $path = 'comments_entity/' . $this->comment_id;
    }

    return array('path' => $path);
  }
}

/**
 * Comments Entity controller class.
 */
class CommentsEntityController extends EntityAPIController {

  public function create(array $values = array()) {
    global $user;
    global $language;

    $values += array(
      'comment_id' => NULL,
      'type' => 'comment',
      'entity_id' => 0,
      'entity_type' => 0,
      'author_entity_type' => 'user',
      'author_entity_id' => $user->uid,
      'hostname' => ip_address(),
      'language' => $language->language,
      'created' => time(),
      'changed' => time(),
      'is_approved' => 0,
      'is_deleted' => 0,
      'is_internal' => 0,
      'status' => 1,
      'data' => array(),
    );

    return parent::create($values);
  }

}
