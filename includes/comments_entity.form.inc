<?php

/**
 * @file
 * Describe comments entity forms.
 */

/**
 * Comments entity delete form
 *
 * @param array $form
 * @param $form_state
 * @param $comment
 * @return mixed
 */
function comments_entity_delete_form($form = array(), $form_state, $comment) {
  return confirm_form(
    $form,
    t('Are you sure you want to delete comment #@comment_id', array('@comment_id' => $comment->comment_id)),
    '',
    t('This comment will be marked as deleted and will no longer be visible.'),
    t('Delete'),
    t('Cancel')
    );
}

/**
 * Comments entity delete form submission
 *
 * @param $form
 * @param $form_state
 */
function comments_entity_delete_form_submit($form, $form_state) {
  $comment = $form_state['build_info']['args'][0];

  if (comments_entity_delete($comment->comment_id)) {
    drupal_set_message(t('Comment deleted'));
  }
}
